
--------------------------------------------------------------------------------
                           SN Bazaarvoice Module
--------------------------------------------------------------------------------

Maintainers: 
 * Divesh Kumar, dkumar32@sapient.com

This module integrates bazaarvoice Rating & Reviews for Drupal. Rating has two 
basic versions offline and active rating. Active rating is the moment when user
provides rating and reviews to a particular node.


Installation
-------------

 * Copy the whole sn_bazaarvoice directory to your modules directory and
   activate the module.


Usage
-----
   
 * Install sn_bazaarvoice module from admin panel.
 
 * Goto url admin/config/system/sn-bazaarvoice.
 
 * Provide the bazaarvoice credentials there. You may also get bazaarvoice cre-
   -dentials from developer.bazaarvoice.com.
   
 * Once required settings are provided, there is another section on the configu-
   -ration page at bottom where you would be selecting the content type on which 
   you want to enable rating and reviews.
   
 * Click on save settings.



--------------------------------------------------------------------------------
                           SN Bazaarvoice Module
--------------------------------------------------------------------------------
Maintainers: 
 * Divesh Kumar, dkumar32@sapient.com
 
This module integrates bazaarvoice Rating & Reviews for Drupal. Rating has two 
basic versions offline and active rating. Active rating is the moment when user
provides rating and reviews to a particular node.


Installation
-------------

 * Once sn_bazaarvoice is installed, just make the configuration changes.


Usage
-----
 * The configurations must be provided with admin/config/system/sn-bazaarvoice.
 
 * Specific node types must be selected to apply bazaarvoice rating for that.


--------------------------------------------------------------------------------
                               General notes
--------------------------------------------------------------------------------
