<?php
/**
 * @file
 * Template file to render single review box.
 */
?>
<div class='review-item'>
  <div class='left'>
    <div class='rating'>
      <?php print theme('sn_bazaarvoice_rating_widget', array('rating_value' => $review['rating'], 'full_node_class' => '', 'nid' => $nid)); ?>
    </div>
    <div class='author'><?php print $review['author'] ?></div>
    <div class='submitted'><?php print $review['submission'] ?></div>
  </div>
  <div class='right'>
    <h2><?php print $review['title'] ?></h2>
    <h5>Rating: <?php print $review['rating']; ?> out of 5</h5>
    <div class='review-text'><?php print $review['review_text'] ?></div>
  </div>
</div>
